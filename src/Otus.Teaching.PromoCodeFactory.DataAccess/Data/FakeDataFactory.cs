﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                AppliedPromocodesCount = 10
            },
        };
        public static IEnumerable<EmployeeRole> EmployeeRoles => new List<EmployeeRole>()
        {
            new EmployeeRole()
            {
                Id = Guid.Parse("67990b00-6e6d-4235-9bcf-72ea86732f70"),
                EmployeeId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                RoleId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
            },
            new EmployeeRole()
            {
                Id = Guid.Parse("d7b8f8d4-5ee7-46fb-9be3-ddbf633c1fe3"),
                EmployeeId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                RoleId = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static IEnumerable<Customer> Customers => new List<Customer>()
        {
            new Customer()
            {
                Id = Guid.Parse("bddc5308-ad0f-4dc3-a49a-d8381e4ece93"),
                FirstName = "Григорий",
                LastName = "Саркисьянц",
            }
        };
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("e4d91da3-d6d6-47c4-965c-944c6633d25f"),
                Name = "Электроника"
            }
        };
        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {
            new PromoCode()
            {
                Id = Guid.Parse("282eaf50-8cdf-4b23-b0c8-11a57b7ce710"),
                Code = "SUMMER20",
                Description = "Промокод 20% на всю электронику до конца лета",
                PreferenceId = Guid.Parse("e4d91da3-d6d6-47c4-965c-944c6633d25f"),
                CustomerId = Guid.Parse("bddc5308-ad0f-4dc3-a49a-d8381e4ece93")
            }
        };
        public static IEnumerable<CustomerPreference> CustomerPreferences => new List<CustomerPreference>()
        {
            new CustomerPreference()
            {
                Id = Guid.Parse("b6518302-4c1a-4e8b-aaa0-acb316c4514a"),
                CustomerId = Guid.Parse("bddc5308-ad0f-4dc3-a49a-d8381e4ece93"),
                PreferenceId =  Guid.Parse("e4d91da3-d6d6-47c4-965c-944c6633d25f"),
            }
        };
    }
}