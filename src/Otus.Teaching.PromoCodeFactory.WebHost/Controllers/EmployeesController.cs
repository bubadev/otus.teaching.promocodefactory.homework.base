﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController: ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<EmployeeRole> _employeeRoleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository, IRepository<EmployeeRole> employeeRoleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
            _employeeRoleRepository = employeeRoleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns>Список сотрудников</returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns>Полная информация о сотруднике</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var roles = await _roleRepository.GetAllAsync();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles != null ? roles.Where(x => employee.Roles.Select(x => x.RoleId).Contains(x.Id)).Select(
                    x => new RoleItemResponse()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description,
                    }).ToList() : new List<RoleItemResponse>(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        public async Task<ActionResult> CreateAsync(EmployeeRequest employeeRequest)
        {
            var roles = await _roleRepository.GetAllAsync();
            if(roles.Any() && employeeRequest.Roles != null)
            {
                if (employeeRequest.Roles.Any(x => !roles.Select(y => y.Id).Contains(x)))
                    return BadRequest("Role not found");
            }
            Employee employee = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Email = employeeRequest.Email,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
            };

            employee.Roles = employeeRequest.Roles != null ? roles.Where(x => employeeRequest.Roles.Contains(x.Id)).Select(
                    x => new EmployeeRole()
                    {
                        EmployeeId = employee.Id,
                        RoleId = x.Id
                    }).ToList() : new List<EmployeeRole>();

            await _employeeRepository.CreateAsync(employee);
            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// 
        /// <param name="id">Guid сотрудника</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            Employee employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
            {
                return NotFound("Employee not found");
            }

            await _employeeRepository.DeleteAsync(id);
            return Ok();
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateAsync(Guid id, EmployeeRequest employeeRequest)
        {
            Employee employee = await _employeeRepository.GetByIdAsync(id);
            if(employee == null)
            {
                return NotFound("Employee not found");
            }

            var roles = await _roleRepository.GetAllAsync();
            if (roles.Any() && employeeRequest.Roles != null)
            {
                if (employeeRequest.Roles.Any(x => !roles.Select(y => y.Id).Contains(x)))
                    return BadRequest("Role not found");
            }

            employee.FirstName = employeeRequest.FirstName;
            employee.LastName = employeeRequest.LastName;
            employee.Email ??= employeeRequest.Email;
            employee.Roles = employeeRequest.Roles != null ? roles.Where(x => employeeRequest.Roles.Contains(x.Id)).Select(
                    x => new EmployeeRole()
                    {
                        EmployeeId = employee.Id,
                        RoleId = x.Id
                    }).ToList() : new List<EmployeeRole>();
            employee.AppliedPromocodesCount 
                = employeeRequest.AppliedPromocodesCount != 0 ? employeeRequest.AppliedPromocodesCount : default;

            await _employeeRepository.UpdateAsync(employee);
            return Ok();
        }
    }
}