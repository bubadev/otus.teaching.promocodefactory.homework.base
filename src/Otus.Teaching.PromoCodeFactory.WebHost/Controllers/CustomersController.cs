﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Покупатели
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IEfRepository<Preference> _preferenceRepository;
        private readonly IEfRepository<PromoCode> _promocodeRepository;

        public CustomersController(ICustomerRepository customerRepository, IEfRepository<Preference> preferenceRepository, IEfRepository<PromoCode> promocodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promocodeRepository = promocodeRepository;
        }
        /// <summary>
        /// Получить данные покупателей
        /// </summary>
        [HttpGet]
        public async Task<List<CustomerResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var preferences = await _preferenceRepository.GetAllAsync();
            var promocodes = await _promocodeRepository.GetAllAsync();
            var result = new List<CustomerResponse>();
            foreach (var customer in customers) 
            {
                var response = new CustomerResponse()
                {
                    Id = customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Preferences = preferences
                        .Where(x => customer.Preferences.Select(x => x.PreferenceId).Contains(x.Id))
                        .ToList(),
                    PromoCodes = customer.PromoCodes
                        .ToList(),
                };

                result.Add(response);
            }

            return result;
        }

        /// <summary>
        /// Получить данные покупателя по Id
        /// </summary>
        /// <returns>Полная информация о покупателе</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var customer = await _customerRepository.GetAsync(id);

            if (customer == null)
                return NotFound();

            var response = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = _preferenceRepository.GetAll()
                    .Where(x => customer.Preferences.Select(x => x.PreferenceId).Contains(x.Id))
                    .ToList(),
                PromoCodes = customer.PromoCodes
                        .ToList(),
            };

            return response;
        }

        /// <summary>
        /// Создать покупателя
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        public async Task<ActionResult> CreateAsync(CustomerRequest customerRequest)
        {
            Customer customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = customerRequest.FirstName,
                LastName = customerRequest.LastName,
            };
            await _customerRepository.AddAsync(customer);
            await _customerRepository.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Удалить покупателя
        /// </summary>
        /// 
        /// <param name="id">Guid покупателя</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            Customer employee = await _customerRepository.GetAsync(id);
            if (employee == null)
            {
                return NotFound("Customer not found");
            }

            _customerRepository.Delete(id);
            _customerRepository.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// Обновить данные покупателя
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> Update(Guid id, CustomerRequest customerRequest)
        {
            Customer customer = await _customerRepository.GetAsync(id);
            if (customer == null)
            {
                return NotFound("Customer not found");
            }

            customer.FirstName = customerRequest.FirstName;
            customer.LastName = customerRequest.LastName;

            _customerRepository.Update(customer);
            _customerRepository.SaveChanges();
            return Ok();
        }
    }
}
