﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IEfRepository<PromoCode> _promocodeRepository;
        public PromoCodesController(IEfRepository<PromoCode> promocodeRepository, ICustomerRepository customerRepository) 
        {
            _promocodeRepository = promocodeRepository;
            _customerRepository = customerRepository;
        }
        /// <summary>
        /// Добавление нового промокода и выдача пользователям
        /// </summary>
        [HttpPost()]
        public async Task<IActionResult> GivePromocodesToCustomersWithPreferenceAsync(PromoCodeRequest promoCodeRequest)
        {
            var promocode = new PromoCode() 
            {
                Id = Guid.NewGuid(),
                Code = promoCodeRequest.Code,
                Description = promoCodeRequest.Description,
                PreferenceId = promoCodeRequest.PreferenceId,
                EndDate = promoCodeRequest.EndDate,
            };
            var customers = await _customerRepository.GetAllAsync();

            customers = customers.Where(x => x.Preferences.Select(x => x.PreferenceId).Contains(promocode.PreferenceId))
                .ToList();

            // Здесь нестыковка в задании, пункт 4:
            // ... будем считать, что в данном примере промокод может быть выдан только одному клиенту из базы ...
            // При этом пункт 7:
            // ... находить клиентов с переданным предпочтением и добавлять им данный промокод ...
            // Непонятно, кому выдавать промокод. Возможно подразумевается настройка связи через Many-to-many таблицу CustomerPromoCodes

            await _promocodeRepository.AddAsync(promocode);
            await _promocodeRepository.SaveChangesAsync();

            return Ok();
        }
        /// <summary>
        /// Список промокодов
        /// </summary>
        [HttpGet()]
        public async Task<List<PromoCodeResponse>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();
            return promocodes.Select(x => new PromoCodeResponse()
            {
                Code = x.Code,
                Description = x.Description,
                PreferenceId = x.PreferenceId.ToString(),
                EndDate = x.EndDate.ToString(),
            }).ToList();
        }
    }
}
