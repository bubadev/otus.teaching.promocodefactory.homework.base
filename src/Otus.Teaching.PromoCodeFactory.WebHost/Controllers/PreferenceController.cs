﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : Controller
    {
        private readonly IEfRepository<Preference> _preferenceRepository;
        public PreferenceController(IEfRepository<Preference> preferenceRepository) 
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Справочник всех предпочтений
        /// </summary>
        /// <returns>PreferenceResponse список предпочтений</returns>
        public List<PreferenceResponse> GetList()
        {
            return  _preferenceRepository.GetAll().Select(x => new PreferenceResponse() { Name = x.Name }).ToList();
        }
    }
}
