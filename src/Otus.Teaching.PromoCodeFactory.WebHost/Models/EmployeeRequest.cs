﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }

        public List<Guid> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}