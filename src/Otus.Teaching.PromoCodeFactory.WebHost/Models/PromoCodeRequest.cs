﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeRequest
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public Guid PreferenceId { get; set; }
        public DateTime EndDate { get; set; }
    }
}
