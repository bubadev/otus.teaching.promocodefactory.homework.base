﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeResponse
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string PreferenceId { get; set; }
        public string EndDate { get; set; }
    }
}
