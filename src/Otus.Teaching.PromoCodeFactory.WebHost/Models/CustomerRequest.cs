﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
