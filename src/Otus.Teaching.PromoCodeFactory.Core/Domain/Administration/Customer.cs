﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Customer : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<CustomerPreference> Preferences { get; set; }
        public List<PromoCode> PromoCodes { get; set; }
    }
}
