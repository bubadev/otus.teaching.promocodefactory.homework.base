﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Preference : BaseEntity
    {
        public string Name { get; set; }
    }
}
