﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class PromoCode : BaseEntity
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public Guid PreferenceId { get; set; }
        public Preference Preference { get; set; }
        public Guid CustomerId { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
