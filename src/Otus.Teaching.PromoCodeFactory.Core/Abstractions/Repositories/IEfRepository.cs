﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IEfRepository<T>  where T : BaseEntity
    {
        T Get(Guid id, bool asNoTracking = false);
        Task<T> GetAsync(Guid id, bool asNoTracking = false);
        IQueryable<T> GetAll(bool noTracking = false);
        Task<List<T>> GetAllAsync(bool asNoTracking = false);

        T Add(T entity);
        Task<T> AddAsync(T entity);
        void AddRange(List<T> entities);
        Task AddRangeAsync(ICollection<T> entities);

        void Update(T entity);

        bool Delete(Guid id);
        bool Delete(T entity);
        bool DeleteRange(ICollection<T> entities);

        void SaveChanges();
        Task SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}